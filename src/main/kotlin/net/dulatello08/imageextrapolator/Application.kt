package net.dulatello08.imageextrapolator

import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.network.tls.certificates.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.slf4j.*
import java.io.File
import java.io.FileInputStream
import java.security.KeyStore
import java.util.concurrent.TimeUnit

fun getRandomString(length: Int) : String {
    val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
    return (1..length)
        .map { allowedChars.random() }
        .joinToString("")
}

fun String.runCommand(workingDir: File = File("/tmp/")): String {
    val process = ProcessBuilder(*split(" ").toTypedArray())
        .inheritIO()
        .directory(workingDir)
        .start()
    if (!process.waitFor(120, TimeUnit.SECONDS)) {
        process.destroy()
        return "Timeout"
    }
    if (process.exitValue() != 0) {
        return "ExitValue"
    }
    return ""
}
val randomString = getRandomString(5)
fun Application.configureRouting() {

    routing {
        post("/api/extrapolateImage") {
            val multipartData = call.receiveMultipart()

            multipartData.forEachPart { part ->
                part as PartData.FileItem
                val fileBytes = part.streamProvider().readBytes()
                File("/tmp/imageExtrapolator-$randomString.jpg").writeBytes(fileBytes)
            }
            when ("python3 ./extrapolator.py $randomString".runCommand()) {
                "Timeout" -> {
                    call.respond(HttpStatusCode.GatewayTimeout, "Timeout")
                }
                "ExitValue" -> {
                    call.respond(HttpStatusCode.InternalServerError, "Error")
                }
                else -> {
                    call.response.header(
                        HttpHeaders.ContentDisposition,
                        ContentDisposition.Attachment.withParameter(ContentDisposition.Parameters.FileName, "output.png")
                            .toString()
                    )
                    call.respondFile(File("/tmp/imageExtrapolator-$randomString.jpg"))
                }
            }
        }
    }
}
var envVar: Int = System.getenv("PORT").toInt()
fun main() {
    val keyStoreFile = FileInputStream("ssl/keystore.jks")
    val keyStore = KeyStore.getInstance(KeyStore.getDefaultType())
    keyStore.load(keyStoreFile, "dulatello".toCharArray())
    val environment = applicationEngineEnvironment {
        log = LoggerFactory.getLogger("ktor.application")
        connector {
            port = envVar
        }
        sslConnector(
            keyStore = keyStore,
            keyAlias = "key0",
            keyStorePassword = { "dulatello".toCharArray() },
            privateKeyPassword = { "dulatello".toCharArray() }
        ){
            port = 8443
        }
        module(Application::configureRouting)
    }
    embeddedServer(Netty, environment).start(wait = true)
}
